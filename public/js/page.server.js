  

  $(function() {

    $("#slides").responsiveSlides();
    $("#voteBox").modal({ 'keyboard': false, 'show': false});
    $('.avatar').tooltip();

	$.ajax({
		url: window.serverurl + "/query",
		timeout: 5000,
		cache: false,
		dataType: 'json'
	}).success(function(response) {
		$('#status').html('<span class="label label-success">Online</span>'); 
		if (typeof response.Players != 'undefined') {
			$('#players').html(response.Players + '/' + response.MaxPlayers);
			$('#version').html(response.Software);
			$('#plugins1').html("");
			$('#playerbar').css('width', Math.round(100* response.Players/response.MaxPlayers) + "%");

			var split = Math.floor(response.Plugins.length * 0.5);

			$.each(response.Plugins, function(i, item) {
				item = item.split(" ");
				$(i >= split ? '#plugins2' : '#plugins1').append("<tr><td>" + item[0] + "</td><td>" + item[1] + "</td></tr>");
			});
		} else {
			$('#players, #version').html('???');
		}
	}).fail(function() {
		$('#status').html('<span class="label label-important">Offline</span>');
		$('#last_up').show();
	});

});