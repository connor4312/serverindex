<?php
define("GDIMAGE_FLIP_HORIZONTAL",	1);
define("GDIMAGE_FLIP_VERTICAL",	2);
define("GDIMAGE_FLIP_BOTH",		3);

class GDImage {
	public $file_name;
	public $info;
	public $width;
	public $height;
	public $image;
	public $org_image;

	function GDImage($image_file) {
		if(!function_exists('imagecreatefrompng')) return;
		if(!file_exists($image_file) or !is_readable($image_file)) return;
		
		$this->file_name = $image_file;
		$img = getimagesize($image_file);

		switch($img['mime']) {
			case 'image/png' : $image = imagecreatefrompng($image_file); break;
			case 'image/jpeg': $image = imagecreatefromjpeg($image_file); break;
			case 'image/gif' : 
				$old_id = imagecreatefromgif($image_file); 
				$image  = imagecreatetruecolor($img[0],$img[1]); 
				imagecopy($image,$old_id,0,0,0,0,$img[0],$img[1]); 
				break;
			default: break;
		}
		$this->info		= $img;
		$this->width	= imagesx($image);
		$this->height	= imagesy($image);
		$this->image	= $this->org_image = $image;
	}
	
	function rotate($angle, $background=0) {
		if(!$this->image) return false;
		if(!$angle) return $this;
		
		$this->image = imagerotate($this->image, $angle, $background);
		return $this;
	}

	function flip($type) {
		if(!$this->image) return false;
		if(!$type) return false;
		
		$imgdest= imagecreatetruecolor($this->width, $this->height);
		$imgsrc	= $this->image;
		$height	= $this->height;
		$width	= $this->width;

		switch( $type ) {
			case GDIMAGE_FLIP_HORIZONTAL:
			case 'h':
				for( $x=0 ; $x<$width ; $x++ )
					imagecopy($imgdest, $imgsrc, $width-$x-1, 0, $x, 0, 1, $height);
				break;

			case GDIMAGE_FLIP_VERTICAL:
			case 'v':
				for( $y=0 ; $y<$height ; $y++ )
					imagecopy($imgdest, $imgsrc, 0, $height-$y-1, 0, $y, $width, 1);
				break;

			case GDIMAGE_FLIP_BOTH:
			case 'b':
				for( $x=0 ; $x<$width ; $x++ )
					imagecopy($imgdest, $imgsrc, $width-$x-1, 0, $x, 0, 1, $height);

				$rowBuffer = imagecreatetruecolor($width, 1);
				for( $y=0 ; $y<($height/2) ; $y++ ) {
					imagecopy($rowBuffer, $imgdest  , 0, 0, 0, $height-$y-1, $width, 1);
					imagecopy($imgdest  , $imgdest  , 0, $height-$y-1, 0, $y, $width, 1);
					imagecopy($imgdest  , $rowBuffer, 0, $y, 0, 0, $width, 1);
				}

				imagedestroy( $rowBuffer );
				break;
			}
		
		$this->image = $imgdest;
		return $this;
	}
	
	function resize($new_width,$new_height, $use_resize = true) {
		if(!$this->image) return false;
		if(!$new_height and !$new_width) return false;
		
		$height = $this->height;
		$width  = $this->width;
		
		if(!$new_height and $new_width) $new_height = $height * $new_width / $width; //Get the new height in the correct ratio
		if($new_height and !$new_width) $new_width	= $width  * $new_height/ $height;//Get the new width in the correct ratio

		//Create the image
		$new_image = imagecreatetruecolor($new_width,$new_height);
		imagealphablending($new_image, false);
		if($use_resize) imagecopyresized($new_image, $this->image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
		else imagecopyresampled($new_image, $this->image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
		
		$this->image = $new_image;
		return $this;
	}


	function crop($from_x,$from_y,$to_x,$to_y) {
		if(!$this->image) return false;
		
		$height = $this->height;
		$width  = $this->width;

		$new_width  = $to_x - $from_x;
		$new_height = $to_y - $from_y;
		//Create the image
		$new_image = imagecreatetruecolor($new_width, $new_height);
		imagealphablending($new_image, false);
		imagecopy($new_image, $this->image, 0,0, $from_x,$from_y, $new_width, $new_height);
		$this->image = $new_image;
		
		return $this;
	}

	function save($file_name, $destroy = true) {
		if(!$this->image) return false;
		
		$extension = strtolower(pathinfo($file_name, PATHINFO_EXTENSION));
		
		switch($extension) {
			case 'png' : return imagepng($this->image, $file_name); break;
			case 'jpeg': 
			case 'jpg' : return imagejpeg($this->image, $file_name); break;
			case 'gif' : return imagegif($this->image, $file_name); break;
			default: break;
		}
		if($destroy) $this->destroy();
		return false;
	}

	function show($destroy = true) {
		if(!$this->image) return false;
		
		header("Content-type: ".$this->info['mime']);
		switch($this->info['mime']) {
			case 'image/png' : imagepng($this->image); break;
			case 'image/jpeg': imagejpeg($this->image); break;
			case 'image/gif' : imagegif($this->image); break;
			default: break;
		}
		if($destroy) $this->destroy();
		
		return $this;
	}

	function restore() {
		$this->image = $this->org_image;
		return $this;
	}

	function destroy() {
		 imagedestroy($this->image);
		 imagedestroy($this->org_image);
	}
}