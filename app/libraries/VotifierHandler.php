<?php

class VotifierHandler {
	public static function Submit($public_key, $server_ip, $server_port, $username) {

		$public_key = chunk_split($public_key, 64, "\n");
		$public_key = "-----BEGIN PUBLIC KEY-----\n$public_key-----END PUBLIC KEY-----";

		$address = $_SERVER['REMOTE_ADDR'];

		$timeStamp = time();

		$string = "VOTE\ntherserverindex.com\n$username\n$address\n$timeStamp\n";

		$leftover = (256 - strlen($string)) / 2;

		while ($leftover > 0) {
		    $string.= "\x0";
		    $leftover--;
		}

		openssl_public_encrypt($string, $crypted,$public_key);

		$socket = fsockopen($server_ip, $server_port, $errno, $errstr, 3);
		if ($socket) {
			fwrite($socket, $crypted);
			return true; 
		}
		else {
			return false;
		}
	}
}