<?php

class IndexController extends BaseController {

	function index() {

		$number_per_page = 15;

		$servers = DB::table('servers')->
			join('images', 'images.server_id', '=', 'servers.id')->
			groupBy('servers.id')->
			orderBy('servers.votes', 'DESC')->
			paginate($number_per_page, array(
				'images.filename',
				'servers.id as id',
				'servers.name',
				'servers.ip',
				'servers.port',
				'servers.website',
				'servers.uptime_success',
				'servers.uptime_fail',
				'servers.votes',
				'servers.country'
			));

		$sponsoreds = DB::table('servers')->
			join('images', 'images.server_id', '=', 'servers.id')->
			groupBy('servers.id')->
			select( array(
				'images.filename',
				'servers.id as id',
				'servers.name',
				'servers.ip',
				'servers.port',
				'servers.website',
				'servers.uptime_success',
				'servers.uptime_fail',
				'servers.votes',
				'servers.country'
			))->
			whereRaw('UNIX_TIMESTAMP(`sponsored`) > UNIX_TIMESTAMP()')->
			take(5)->
			get();

		return View::make('standard')->
			with('title', 'Listing Page' . $servers->getCurrentPage())->
			nest('content', 'pages.index', array(
				'servers' => $servers,
				'sponsoreds' => $sponsoreds,
				'ppage' => $number_per_page
			));
	}
	

}