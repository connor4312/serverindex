<?php
class ServerEditController extends BaseController {

	protected $tags = array(

			'[b]' => '<b>',
			'[/b]' => '</b>',
			'[i]' => '<i>',
			'[/i]' => '</i>',
			'[u]' => '<u>',
			'[/u]' => '</u>',
			'--' => '&bull;&nbsp;&nbsp;',
			"\n" => '<br />'
		);

	public function createServer($edit = null) {
		if (!Auth::check())	{
			return Redirect::to('/account/login');
		} elseif ($s = Auth::user()->server && !$edit) {
			return Redirect::to('/server/' . $s->id);
		}

		if (Input::get('clearimg')) {
			$image = Auth::user()->images()->get();
			
			foreach ($image as $im) {
				unlink(public_path() . '/uploads/' . $im->filename);
			}

			DB::table('images')->where('user_id', '=', Auth::user()->id)->delete();
		}

		$im = Auth::user()->images()->get();

		return View::make('standard')->
			with('title', 'Create Server')->
			with('header_css', array('css/basic.css'))->
			with('footer_scripts', array('js/dropzone.js'))->
			nest('content', 'pages.makeserver', array('images' => $im, 'edit' => $edit));
	}

	public function postServer() {


		if (!Auth::check())	{
			return Redirect::to('/account/login');
		} elseif ($s = Auth::user()->server && !Input::get('edit')) {

			return Redirect::to('/server/' . $s->id);

		} elseif (Input::get('edit')) {

			$s = Server::find(Input::get('edit'));

			if (!$s) {
				return App::abort(404);
			}

			$user = $s->user;

			if (Auth::user()->id != $user->id && Auth::user()->role != 'superuser') {
				return Redirect::to('account/login');
			}

		}

		$im = Auth::user()->images()->get();

		if (!count($im) && !Input::get('edit')) {
			return View::make('standard')->
				with('title', 'Create Server')->
				with('header_css', array('css/basic.css'))->
				with('footer_scripts', array('js/dropzone.js'))->
				nest('content', 'pages.makeserver', array('images' => $im, 'alert' => '<div class="alert">You must upload at least one server image</div>'));
		}

		$input = Input::all();

		$rules = array(
			'name' => 'required|between:5,50' . (Input::get('edit') ? '' : '|unique:servers'),
			'ip' => 'required|between:5,45' . (Input::get('edit') ? '' : '|unique:servers'),
			'query_port' => 'required|numeric',
			'website' => 'url',
			'description' => 'required|between:50,2000',
			'country' => 'alpha_num',
			'vf-port' => 'numeric|required_with:vf-port',
			'vf-public' => 'required_with:vf-public',
		);
	 
		$validation = Validator::make($input, $rules);

		$error = '';

		if ($validation->fails()) {
			$messages = $validation->messages();

			$error = '<div class="alert">';
			foreach ($messages->all() as $message) {
				$error .= $message . ' ';
			}
			$error .= '</div>';

			$im = Auth::user()->images()->get();
			return View::make('standard')->
				with('title', 'Create Server')->
				with('header_css', array('css/basic.css'))->
				with('footer_scripts', array('js/dropzone.js'))->
				nest('content', 'pages.makeserver', array('images' => $im, 'alert' => $error, 'edit' => Input::get('edit')));
		}

		$ip = explode(':', htmlspecialchars(Input::get('ip')));

		$server = Input::get('edit') ? $s : new Server();
		$server->name = htmlspecialchars(Input::get('name'));
		$server->ip = $ip[0];
		$server->port = isset($ip[1]) ? $ip[1] : '25565';
		$server->query_port = Input::get('query_port');
		$server->plugins = Input::get('plugins') == 'yes' ? 1 : 0;

		$description = Input::get('description');
		$description = htmlspecialchars($description);

		foreach ($this->tags as $bb => $html) {
			$description = str_replace($bb, $html, $description);
		}

		$server->description = $description;
		$server->country = Input::get('country');
		$server->website = Input::get('website');

		if (!Input::get('edit')) {
			$server->user_id = Auth::user()->id;
			$server->last_up = time();
		}

		DB::table('images')->
			where('user_id', '=', $server->user_id)->
			update(array('server_id' => $server->id));

		$server->save();

		if (Input::get('vf-port')) {
			if (!Input::get('edit') || !$vf = Votifier::where('server_id', '=', $server->id)->first()) {
				$vf = new Votifier();
			}
			$vf->server_id = $server->id;
			$vf->ip = $server->ip;
			$vf->port = Input::get('vf-port');
			$vf->public_key = Input::get('vf-public');
			$vf->save();
		}

		return Redirect::to('/server/' . $server->id);
	}

	public function uploadImg() {
		if (!Auth::check())	{
			return Redirect::to('/account/login');
		}

		$images = Auth::user()->images->count();

		if ($images > 3) {
			return Response::make('Already has four images uploaded.', 400);
		}
 
		$input = Input::all();
		$rules = array(
			'file' => 'required|image|max:3000',
		);
	 
		$validation = Validator::make($input, $rules);
	 
		if ($validation->fails()) {
			$out = '';
			foreach ($validation->messages() as $m) {
				$out .= $m . ' ';
			}
			return Response::make($out, 400);
		}
	 
		$file = Input::file('file');
	 
		$extension = File::extension($file->getClientOriginalName());

		$directory = public_path() . '/uploads';

		$filename = Auth::user()->id . '-' . sha1(microtime().time()) . '.' . $extension;
	 
		$upload_success = $file->move(storage_path(), $filename);
	 
		if ($upload_success) {

			$img = new GDImage(storage_path() . '/' . $filename);
			if ($images == 0 && $img->width == 468 && $img->height == 60) {
				rename(storage_path() . '/' . $filename, $directory . '/' . $filename);
			} else {
				if ($img->height * 5.11 > $img->width) {
					$img->resize(460, 0);
				} else {
					$img->resize(0, 90);
				}

				$img->crop(0, 0, 460, 90)->save($directory . '/' . $filename);
				unlink(storage_path() . '/' . $filename);
			}

			$image = new Image();
			$image->filename = $filename;
			$image->user_id = Auth::user()->id;
			$image->save();

			return Response::json('success', 200);
		} else {
			return Response::json('error', 400);
		}
	}

	public function edit($id) {

		if (!$server = Server::find($id)) {
			return App::abort(404);
		}

		if (Input::get('clearimg')) {
			$image = Auth::user()->images()->get();
			
			foreach ($image as $im) {
				@unlink(public_path() . '/uploads/' . $im->filename);
			}

			DB::table('images')->where('user_id', '=', Auth::user()->id)->delete();
		}

		$user = $server->user;

		if (!Auth::check()) {
			return Redirect::to('account/login');
		} elseif (Auth::user()->role == 'superuser') {
			$role = 100;
		} elseif (Auth::user()->id == $user->id) {
			$role = 10;
		} else {
			return Redirect::to('account/login');
		}

		$in['name'] = htmlspecialchars_decode($server->name);
		$in['ip'] = $server->ip . ':' . $server->port;


		$description = $server->description;

		foreach ($this->tags as $bb => $html) {
			$description = str_replace($html, $bb, $description);
		}

		$description = htmlspecialchars_decode($description);

		$in['description'] = $description;
		$in['country'] = $server->country;

		$vf = $server->votifier()->first();

		$in['vf-port'] = $vf['port'];
		$in['vf-public'] = $vf['public_key'];

		$in['website'] = $server->website;
		$in['query_port'] = $server->query_port;

		Input::replace($in);

		return $this->createServer($id);

	}
}
?>