<?php

class ServerController extends BaseController {

	protected $vote_hours_cooldown = 24;
	
	public function view($id, $error = '') {

		if (!$server = Server::find($id)) {
			return App::abort(404);
		}
		$user = $server->user;

		if (!Auth::check()) {
			$role = 0;
		} elseif (Auth::user()->role == 'superuser') {
			$role = 100;

			if (isset($_GET['unsponsor'])) {
				$server->sponsored = date("Y-m-d H:i:s" , time() - 1);
				$server->save();
			}

		} elseif (Auth::user()->id == $user->id) {
			$role = 10;
		} else {
			$role = 0;
		}

		$vote_people = $server->votes()->take(25)->get();
		$votes = $server->votes()->count();
		$images = $server->image()->get();
		$votifier = $server->votifier()->count();

		$hasvoted = $server->votes()->
			where('created_at', '>', DB::raw('DATE_SUB(NOW(), INTERVAL 1 DAY)'))->
			where('ip', '=', $_SERVER['REMOTE_ADDR'])->
			count();
//var_dump(DB::getQueryLog());
//die();
		if (time() - Session::get('voted' . $id) < 3600 * $this->vote_hours_cooldown) {
			$hasvoted ++;
		}

		return View::make('standard')->
			with('title', $server->name)->
			with('footer_scripts', array(
				'js/jquery.slides.min.js',
				'js/page.server.js'
			))->
			nest('content', 'pages.server', array(
				'server' => $server,
				'user' => $user,
				'role' => $role,
				'votes' => $votes,
				'vote_people' => $vote_people,
				'images' => $images,
				'votifier' => $votifier,
				'hasvoted' => $hasvoted,
				'error' => $error,
				'id' => $id
			));
	}

	public function query($id) {

		if (!Request::ajax() || !$server = Server::find($id)) {
			return App::abort(404);
		}


		if (!Cache::has('query-' . $id)) {
			$query = new MinecraftQuery();

			try {

				$query->Connect($server->ip, $server->query_port);

				function regularize($item) {
					foreach ($item as &$i) {
						if (is_array($i)) {
							$i = regularize($i);
						} else {
							$i = preg_replace('/[^(\x20-\x7F)]*/','', $i);
						}
					}
					return $item;
				}

				$info = regularize($query->GetInfo());

				Cache::put('query-' . $id, serialize($info), 1);

				return Response::json($info, 200);

			} catch (Exception $e) {

				if ($fp = fsockopen($server->ip, $server->port, $errCode, $errStr,1)) {
					fclose($fp);
					return Response::json(array(), 200);

				}
				return Response::json('error', 400);
			}
		} else {
			return Response::json(unserialize(Cache::get('query-' . $id)), 200);
		}

	}
	public function vote($id) {

		if (!$server = Server::find($id)) {
			return App::abort(404);
		}

		$hasvoted = $server->votes()->
			where('created_at', '>', DB::raw('DATE_SUB(NOW(), INTERVAL 1 DAY)'))->
			where(function($query){
				$query->where('ip', '=', $_SERVER['REMOTE_ADDR'])->
					orWhere('username', '=', Input::get('username'));
			})->
			count();

		if (time() - Session::get('voted' . $id) < 3600 * $this->vote_hours_cooldown) {
			$hasvoted ++;
		}

		if ($hasvoted) {
			return $this->view($id, '<div class="alert">You already voted!</div>');
		}

		$input = Input::all();
		$validator = Validator::make($input, array(
			'username' => 'required|between:3,50|alpha_dash',
			'recaptcha_response_field' => 'required|recaptcha'
		));

		if ($validator->fails()) {
			$messages = $validator->messages();
			$out = '';
			foreach ($messages->all() as $message) {
				$out .= $message . ' ';
			}

			return $this->view($id, '<div class="alert">' . $out . '</div>');
		}

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'http://minecraft.net/haspaid.jsp?user=' . urlencode(Input::get('username')));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);
		$response = json_decode(curl_exec($ch), true);
		curl_close($ch);

		if ($response == false) {
			return $this->view($id, '<div class="alert">Bad username!</div>');
		}

		$vote = new Vote();
		$vote->server_id = $id;
		$vote->username = Input::get('username');
		$vote->ip = $_SERVER['REMOTE_ADDR'];
		$vote->save();

		Session::put('voted' . $id, time());

		$server->votes = $server->votes + 1;
		$server->save();

		if ($vf = $server->votifier) {
			
			if (!VotifierHandler::Submit($vf->public_key, $vf->ip, $vf->port, Input::get('username'))) {

				$fail = new VFFail();
				$fail->votifier_id = $vf->id;
				$fail->username = Input::get('username');
				$fail->save();

				return $this->view($id, '<div class="alert-success">Vote recorded, but Votifier didn\'t catch it! We will try submitting again in five minutes...</div>');
			}
		}

		return $this->view($id, '<div class="alert-success">You have voted!</div>');
	}

	public function delete($id) {
		if (!Auth::check())	{
			return Redirect::to('/account/login');
		}

		$s = Server::find($id);

		if (!$s) {
			return App::abort(404);
		}

		$user = $s->user;

		if (Auth::user()->id != $user->id && Auth::user()->role != 'superuser') {
			return Redirect::to('/account/login');
		}

		$s->delete();

		return Redirect::to('/');
	}

	public function ban($id) {

		if (!Auth::check())	{
			return Redirect::to('/account/login');
		} elseif (Auth::user()->role != 'superuser') {
			return Redirect::to('/account/login');

		}
		$s = Server::find($id);

		if (!$s) {
			return App::abort(404);
		}

		$s->delete();

		Auth::user()->status = 'banned';
		Auth::user()->save();

		return Redirect::to('/');

	}

	public function sponsor($id) {

		if (Auth::user()->role != 'superuser') {
			return App::abort(404);
		}

		$input = Input::all();
		$rules = array(
			'duration' => 'required|numeric',
		);
	 
		$validation = Validator::make($input, $rules);
	 
		if ($validation->fails()) {
			$out = '';
			foreach ($validation->messages() as $m) {
				$out .= $m . ' ';
			}
			return $this->view($id, '<div class="alert">' . $out . '</div>');
		}

		$s = Server::find($id);

		if (!$s) {
			return App::abort(404);
		}

		$s->sponsored = date("Y-m-d H:i:s" , time() + (Input::get('duration') * 60 * 60 * 24));
		$s->save();

		return $this->view($id, '<div class="alert alert-success">Server sponsored for ' . Input::get('duration') . ' days!</div>');

	}
}