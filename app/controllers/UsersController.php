<?php
class UsersController extends BaseController {

	public function index($error = null) {
		$users = DB::table('users')->
			select('username', 'email', 'role', 'status', 'created_at', 'id')->
			paginate(50);

		return View::make('standard')->
			with('title', 'Users Page' . $users->getCurrentPage())->
			nest('content', 'pages.users', array(
				'users' => $users,
				'error' => $error
			));
	}

	public function view($id) {

		if (!Request::ajax()) {
			return $this->index();
		}

		if (!$user = User::find($id)) {
			return App::abort(404);
		}

		return View::make('pages.get_user')->
			with('user', $user);
	}

	public function register() {
		
		$input = Input::all();
		$validator = Validator::make($input, array(
			'email' => 'required|email|unique:users',
			'username' => 'required|unique:users|between:5,50|alpha_dash',
			'password'  => 'required|between:5,50'
		));
		if ($validator->fails()) {
			$messages = $validator->messages();
			$out = '';
			foreach ($messages->all() as $message) {
				$out .= $message . ' ';
			}
		
			return $this->index($out);

		} else {
			$user = new User();
			$user->email = Input::get('email');
			$user->username = Input::get('username');
			$user->password = Hash::make(Input::get('password'));
			$user->status = 'active';
			$user->save();

			return $this->index();
		}
	}

	public function edit($id) {
		if (!$user = User::find($id)) {
			return $this->index('No user found');
		}

		$user->email = Input::get('email');
		$user->username = Input::get('username');
		if (strlen(Input::get('password')) > 3) {
			$user->password = Hash::make(Input::get('password'));
		}
		$user->status = Input::get('status');
		$user->role = Input::get('role');
		$user->save();

		return $this->index();

	}

}