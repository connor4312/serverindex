<?php
class LoginController extends BaseController {

	public function index() {
		
		$logalert = null;

		if (Auth::check()) {
			Auth::logout();
		}

		if (Input::get('username')) {

			if (Auth::attempt(array(
				'username' => Input::get('username'),
				'password' => Input::get('password'),
				'status' => 'active'
			))) {


				return Redirect::to('/index');

			} else {
				$logalert = '<div class="alert alert-error">Invalid username or password.</div>';
			}
		}

		return View::make('standard')->
			with('title', 'Login')->
			with('footer_scripts', array('js/page.login.js'))->
			nest('content', 'pages.login', array('logalert' => $logalert));
	}

	public function register() {

		$input = Input::all();
		$validator = Validator::make($input, array(
			'email' => 'required|email|unique:users',
			'username' => 'required|unique:users|between:5,50|alpha_dash',
			'password'  => 'required|between:5,50',
			'recaptcha_response_field' => 'required|recaptcha'
		));
		if ($validator->fails()) {
			$messages = $validator->messages();
			$out = '';
			foreach ($messages->all() as $message) {
				$out .= $message . ' ';
			}
		
			return View::make('standard')->
				with('title', 'Login')->
				nest('content', 'pages.login', array('regalert' => '<div class="alert alert-error">' . $out . '</div>'));

		} else {
			$user = new User();
			$user->email = Input::get('email');
			$user->username = Input::get('username');
			$user->password = Hash::make(Input::get('password'));
			$user->status = 'active';
			$user->save();

			Auth::login($user);

			return Redirect::to('/');
		}
	}

	public function forgot() {

		$logalert = null;
		$user = null;

		try {
			$user = User::where('username', '=', Input::get('username'))->firstOrFail();
		} catch (Exception $e) {
			$logalert = '<div class="alert">No user found with those credentials.</div>';
		}

		if ($user) {
			$password = substr(sha1(microtime() . mt_rand()), 0, 10);
			$user->password = Hash::make($password);
			$user->save();

			$data = array();
			$data['email'] = $user->email;
			$data['subject'] = 'Password Reset';
			$data['password'] = $password;

			Mail::send('emails.auth.passreset', $data, function ($message) use ($data) {

				$message->to($data['email'])->subject($data['subject']);

				$message->from(Config::get('mail.from.address'), Config::get('mail.from.name'));
			});


			$logalert = '<div class="alert alert-info">New password sent to your email!</div>';
		}

		return View::make('standard')->
			with('title', 'Login')->
			with('footer_scripts', array('js/page.login.js'))->
			nest('content', 'pages.login', array('logalert' => $logalert));
	}
}
?>