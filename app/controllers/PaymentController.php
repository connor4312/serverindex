<?php 

class PaymentController extends BaseController {
	public function PPHttpPost($methodName_, $nvpStr_) {

			$PayPalMode         = 'live';
			$PayPalApiUsername  = '';
			$PayPalApiPassword  = '';
			$PayPalApiSignature     = '';
			$PayPalCurrencyCode     = 'USD';
			$PayPalReturnURL    = URL::to('/paypal/enter');
			$PayPalCancelURL    = URL::to('/paypal/enter');


			$API_UserName = urlencode($PayPalApiUsername);
			$API_Password = urlencode($PayPalApiPassword);
			$API_Signature = urlencode($PayPalApiSignature);

			$str = '';
			foreach ($nvpStr_ as $key => $nvp) {
				$str .= '&' . $key . '=' . urlencode($nvp);
			}

			if($PayPalMode=='sandbox')
			{
				$paypalmode     =   '.sandbox';
			}
			else
			{
				$paypalmode     =   '';
			}

			$API_Endpoint = "https://api-3t".$paypalmode.".paypal.com/nvp";
			$version = urlencode('76.0');

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $API_Endpoint);
			curl_setopt($ch, CURLOPT_VERBOSE, 1);

			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POST, 1);

			$nvpreq = "METHOD=$methodName_&VERSION=$version&PWD=$API_Password&USER=$API_UserName&SIGNATURE=$API_Signature$str";

			curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);

			$httpResponse = curl_exec($ch);

			if(!$httpResponse) {
				exit("$methodName_ failed: ".curl_error($ch).'('.curl_errno($ch).')');
			}

			$httpResponseAr = explode("&", $httpResponse);

			$httpParsedResponseAr = array();
			foreach ($httpResponseAr as $i => $value) {
				$tmpAr = explode("=", $value);
				if(sizeof($tmpAr) > 1) {
					$httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
				}
			}

			if((0 == sizeof($httpParsedResponseAr)) || !array_key_exists('ACK', $httpParsedResponseAr)) {
				exit("Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");
			}

		return $httpParsedResponseAr;
	}

	public function PaymentController() {


		$this->data = array(
			'PAYMENTREQUEST_0_AMT' => Config::get('app.sponsor_price_per_month'),
			'PAYMENTREQUEST_0_CURRENCY' => 'USD',
			'RETURNURL' => URL::to('/paypal/enter'),
			'CANCELURL' => URL::to('/paypal/enter'),
			'NOSHIPPING' => 1,
			'REQCONFIRMSHIPPING' => 0,
			'L_PAYMENTREQUEST_0_ITEMCATEGORY0' => 'Digital', 
			'L_PAYMENTREQUEST_0_NAME0' => '30 Days Sponsorship',
			'L_PAYMENTREQUEST_0_DESC0' => 'Sponsored status on TheServerIndex for 30 days.',
			'L_PAYMENTREQUEST_0_AMT0' => Config::get('app.sponsor_price_per_month'),
			'L_PAYMENTREQUEST_0_QTY0' => 1,
			'L_PAYMENTREQUEST_0_TAXAMT0' => 0,
		);
	}

	public function exitpage() {

		if (!Auth::check()) {
			return Redirect::to('/account/login');
		} else {
			$user = Auth::user();
		}

		if (!$server = $user->server) {
			return Redirect::to('/account/server');
		}

		if (strtotime($server->sponsored) > time()) {
			return Redirect::to('/account/server');
		}

		$out = $this->PPHttpPost('SetExpressCheckout', $this->data);

		if (strtoupper($out["ACK"]) == "SUCCESS" || strtoupper($out["ACK"]) == "SUCCESSWITHWARNING") {

			return Redirect::to('https://www.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=' . $out["TOKEN"]);
		} else {

			return View::make('standard')->
				with('title', 'Sponsor Server')->
				nest('content', 'pages.sponsor', array(
					'level' => 'warning',
					'message' => 'Error processing payment! You have not been charged, please try again.'
				));

		}
	}

	public function enter() {

		if (!Auth::check()) {
			return Redirect::to('/account/login');
		} else {
			$user = Auth::user();
		}

		if (!$server = $user->server) {
			return Redirect::to('/account/server');
		}

		if (strtotime($server->sponsored) > time()) {
			return Redirect::to('/account/server');
		}

		$data = array(
			'TOKEN' => Input::get('token'),
			'PAYERID' => Input::get('PayerID'),
			'PAYMENTACTION' => 'SALE',
			'AMT' => Config::get('app.sponsor_price_per_month'),
			'CURRENCYCODE' => 'USD'
		);

		$out = $this->PPHttpPost('DoExpressCheckoutPayment', $data);

		if (strtoupper($out["ACK"]) == "SUCCESS" || strtoupper($out["ACK"]) == "SUCCESSWITHWARNING") {

			$server->sponsored = date('Y-m-d H:i:s', time() + (30 * 24 * 60 * 60));
			$server->save();

			return Redirect::to('/account/server');
		} else {
			return View::make('standard')->
				with('title', 'Sponsor Server')->
				nest('content', 'pages.sponsor', array(
					'level' => 'warning',
					'message' => 'Error processing payment! You have not been charged, please try again.'
				));
		}
	}
}