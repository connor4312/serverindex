<?php

class AccountController extends BaseController {
	public function view() {

		if (!Auth::check()) {
			return Redirect::to('/account/login');
		}

		return View::make('standard')->
			with('title', 'Edit Account')->
			nest('content', 'pages.accedit', array('user' => Auth::user()));
	}

	public function edit() {

		if (!Auth::check()) {
			return Redirect::to('/account/login');
		}

		$user = Auth::user();

		if (!Auth::validate(array(
				'username' => $user->username,
				'password' => Input::get('currentpassword')
			))) {

			return View::make('standard')->
				with('title', 'Edit Account')->
				nest('content', 'pages.accedit', array('user' => $user, 'msg' => 'You must enter your current password!'));
		}

		$input = Input::all();
		$validator = Validator::make($input, array(
			'email' => 'required|email',
			'password'  => 'between:5,50'
		));
		if ($validator->fails()) {
			$messages = $validator->messages();
			$out = '';
			foreach ($messages->all() as $message) {
				$out .= $message . ' ';
			}
		
			return View::make('standard')->
				with('title', 'Edit Account')->
				nest('content', 'pages.accedit', array('user' => $user, 'msg' => $out));
		
		}

		$user->email = Input::get('email');
		if (Input::get('password')) {
			$user->password = Hash::make(Input::get('password'));
		}

		$user->save();

		return View::make('standard')->
			with('title', 'Edit Account')->
			nest('content', 'pages.accedit', array('user' => $user));

	}
}