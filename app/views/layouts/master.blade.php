<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>TheServerIndex - {{ $title }}</title>
		{{ HTML::style('css/style.css') }}
		<?php if (isset($header_css)) {
			foreach ($header_css as $css) {
				echo HTML::style($css) . "\n";
			}
		}
		?>
	</head>
	<body><div id="page">
		<div id="navbar">
			<div class="container">
				<h1><a href="{{ URL::to('/') }}" id="logo">MySite<span>Name</span></a></h1>
				<ul>
					<li>{{ HTML::link('/', 'Home') }}</a></li>
					<li>{{ HTML::link('/account/server', 'My Server') }}</a></li>
					<li>{{ HTML::link('/account/server', 'Sponsor') }}</a></li>
					@if (Auth::check())
					<li>{{ HTML::link('/account/edit', 'My Account') }}</a></li>
					<li>{{ HTML::link('/account/logout', 'Logout') }}</a></li>
					@else
					<li>{{ HTML::link('/account/login', 'Login') }}</a></li>
					@endif
				</ul>
			</div>
		</div>
			
			@yield('content')
			
			<div id="fpush"></div>
		</div>
		<div id="footer">
			<div class="container">
				<div class="pull-left">
					Copyright 2013 
				</div>
				<div class="pull-right">
					<a href="#" id="tos">Terms of Service</a> | <a href="http://connorpeet.com" class="light">Design &amp; Development</a>
				</div>
			</div>
		</div>
		@include ('layouts.scripts')
	</body>
</html>