	{{ HTML::script('//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js') }}
	{{ HTML::script('/js/bootstrap.min.js') }}
	<script type="text/javascript">try{Typekit.load();}catch(e){} window.baseurl = '{{ URL::to('/') }}';$('#tos').click(function(){$.get('{{ URL::to('/tos') }}', function(data){$('body').append(data);$('#tosmod').modal({show: true});});});</script>
	<?php if (isset($footer_scripts)) {
		foreach ($footer_scripts as $script) {
			echo HTML::script($script) . "\n";
		}
	}
	?>