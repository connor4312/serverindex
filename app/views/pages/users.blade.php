<div class="container" id="content-standard">
	<div class="row">
		<div class="span8 boxxed">
			<div class="padded">
				<h1>Existing Users</h1>
				{{ $error ? '<div class="alert">' . $error . '</div>' : ''}}
				<table class="table tabled-striped table-bordered">
					<thead>
						<td>Username</td>
						<td>Email</td>
						<td>Role</td>
						<td>Status</td>
						<td>Registered</td>
						<td>&nbsp;</td>
					</thead>
					<tbody>
						@foreach ($users as $user)
						<tr>
							<td>{{ $user->username }}</td>
							<td>{{ $user->email }}</td>
							<td>{{ $user->role }}</td>
							<td>{{ $user->status }}</td>
							<td><{{ date('F js', strtotime($user->created_at)) }}</td>
							<td>
								{{ HTML::link('/users/' . $user->id . '/view', 'Edit', array('class' => 'btn', 'data-toggle' => 'modal', 'data-target' => '#userModual')) }}
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
				{{ $users->links() }}
			</div>
		</div>
		<div class="span4 boxxed">
			<div class="padded">
				<h1>Create New User</h1>
				{{ Form::open(array('url' => '/users/register', 'method' => 'POST', 'class' => 'form-horizontal')) }}
				<table class="table table-striped">
					<tr>
						<td>
						{{ Form::label('username', 'Username') }}
						</td>
						<td>
							{{ Form::text('username') }}
						</td>
					</tr>
					<tr>
						<td>
						{{ Form::label('email', 'Email') }}
						</td>
						<td>
							{{ Form::text('email') }}
						</td>
					</tr>
					<tr>
						<td>
						{{ Form::label('password', 'Password') }}
						</td>
						<td>
							{{ Form::password('password') }}
						</td>
					</tr>
					<tr>
						<td>
						</td>
						<td>
							<input type="submit" class="btn btn-primary" id="registerbtn" value="Register">
						</td>
					</tr>
				</table>
				{{ Form::close() }}
			</div>
		</div>
	</div>
</div>

<div id="userModual" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="userModualLabel" aria-hidden="true">
	<div class="modal-header">
		<h3 id="userModualLabel">Edit User</h3>
	</div>
	<div class="modal-body">
		
	</div>
</div>