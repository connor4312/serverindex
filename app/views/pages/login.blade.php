<div class="container" id="content-standard">
	<div class="row">
		<div class="span6">
			<div class="padded boxxed">
				<h1>Login</h1>
				{{ isset($logalert) && !is_null($logalert) ? $logalert : '' }}
				{{ Form::open(array('url' => '/account/login', 'method' => 'POST', 'class' => 'form-horizontal', 'id' => 'login')) }}
					<div class="control-group">
						{{ Form::label('username', 'Username', array('class' => 'control-label')) }}
						<div class="controls">
							{{ Form::text('username', '', array('id' => 'username')) }}
						</div>
					</div>
					<div class="control-group">
						{{ Form::label('password', 'Password', array('class' => 'control-label')) }}
						<div class="controls">
							{{ Form::password('password') }}
						</div>
					</div>
					<div class="control-group">
						<div class="controls">
							<input type="submit" class="btn btn-primary" value="Login">
							<a class="btn" id="forgotbtn">Forgot Password</a>
						</div>
					</div>
				{{ Form::close() }}
			</div>
		</div>
		<div class="span6">
			<div class="padded boxxed">
			<h1>Register</h1>
				{{ isset($regalert) && !is_null($regalert) ? $regalert : '' }}
				{{ Form::open(array('url' => '/account/register', 'method' => 'POST', 'class' => 'form-horizontal')) }}
					<div class="control-group">
						{{ Form::label('username', 'Username', array('class' => 'control-label')) }}
						<div class="controls">
							{{ Form::text('username') }}
						</div>
					</div>
					<div class="control-group">
						{{ Form::label('email', 'Email', array('class' => 'control-label')) }}
						<div class="controls">
							{{ Form::text('email') }}
						</div>
					</div>
					<div class="control-group">
						{{ Form::label('password', 'Password', array('class' => 'control-label')) }}
						<div class="controls">
							{{ Form::password('password') }}
						</div>
					</div>
					<div class="control-group">
						<div class="controls">
							{{ Form::captcha() }}
						</div>
					</div>
					<div class="control-group">
						<div class="controls">
							<input type="submit" class="btn btn-primary" id="registerbtn" value="Register">
						</div>
					</div>
				{{ Form::close() }}
			</div>
		</div>
	</div>
</div>