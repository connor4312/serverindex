		<div id="intro">
			<div class="container">
				<div id="introcont">
					<h1>The Best of the Best</h1>
						@foreach ($sponsoreds as $sponsored)
							
						<div class="container serverlisting row">
							<div class="span3">
								<h3><div>{{ $sponsored->name }}</div>
									<span>{{number_format(round(100 * $sponsored->uptime_success / ($sponsored->uptime_success + $sponsored->uptime_fail), 2), 2) }}%</span>
								</h3>
								<code id="ip-srv{{ $sponsored->id }}">{{ $sponsored->ip . ($sponsored->port == 25565 ? '' : ':' . $sponsored->port) }}</code>
							</div>
							<div class="span1">
								<a class="playlink blue" OnClick="prompt('Copy this Address:','{{ $sponsored->ip . ($sponsored->port == 25565 ? '' : ':' . $sponsored->port) }}')">Play</a>
								<a class="weblink gray" href="{{ URL::to('/server/' . $sponsored->id ) }}">View</a>
							</div>
							<div class="span5">
								<div class="server-img standard" style="background:url('{{ URL::to('/') . '/uploads/' . $sponsored->filename }}')"></div>
							</div>
							<div class="span1">
								<div class="votebox">
									<span>{{ $sponsored->votes }}</span>
									<a href="{{ URL::to('/server/' . $sponsored->id ) }}" class="gray">Vote</a>
								</div>
							</div>

						</div>

						@endforeach
				</div>
			</div>
		</div>
		@for ($i = 0; $i < count($servers); $i++)
			
		<div class="container serverlisting row">
			<div class="span2">
				{{ str_pad((($servers->getCurrentPage() - 1) * $ppage + $i + 1), 4, '0', STR_PAD_LEFT) }}
			</div>
			<div class="span3">
				<h3><div>{{ $servers[$i]->name }}</div>
					<span>{{number_format(round(100 * $servers[$i]->uptime_success / ($servers[$i]->uptime_success + $servers[$i]->uptime_fail), 2), 2) }}%</span>
				</h3>
				<code id="ip-srv{{ $servers[$i]->id }}">{{ $servers[$i]->ip . ($servers[$i]->port == 25565 ? '' : ':' . $servers[$i]->port) }}</code>
			</div>
			<div class="span1">
				<a class="playlink blue"  OnClick="prompt('Copy this Address:','{{ $servers[$i]->ip . ($servers[$i]->port == 25565 ? '' : ':' . $servers[$i]->port) }}')">Play</a>
				<a class="weblink gray" href="{{ URL::to('/server/' . $servers[$i]->id ) }}">View</a>
			</div>
			<div class="span5">
				<div class="server-img standard" style="background:url('{{ URL::to('/') . '/uploads/' . $servers[$i]->filename }}')"></div>
			</div>
			<div class="span1">
				<div class="votebox">
					<span>{{ $servers[$i]->votes }}</span>
					<a href="{{ URL::to('/server/' . $servers[$i]->id ) }}" class="gray">Vote</a>
				</div>
			</div>

		</div>

		@endfor
		{{ $servers->links() }}
		<div class="clear"></div>