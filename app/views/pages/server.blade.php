<?php use Carbon\Carbon; ?>
<div class="container" id="content-standard">
	<div class="row">
		<h1>{{ $server->name }}</h1>
		<div class="span3">
			<div class="padded boxxed">
				{{ $error }}
				<div id="vote-container">
					@if ($hasvoted)
					<a role="button" class="btn btn-primary btn-large disabled">You Already Voted</a>
					@else
					<a href="#voteBox" class="btn btn-primary btn-large" data-toggle="modal">Vote Now!</a>
					@endif
				</div>
			</div>

			<div class="boxxed">
				<div class="padded"><h2>Server Info
				@if (strtotime($server->sponsored) > time())
				<span class="label label-success">Sponsored</span>
				@endif
				</h2></div>
				@if ($role)
				<div class="alert alert-info">
					Don't see anything here? Be sure <code>enable-query=true</code> in your server.properties!
				</div>
				@endif
				<table class="table table-striped">
					<tr>
						<th>Status</td>
						<td class="ajaxd" id="status"><i class="muted">Loading</i></td>
					</tr>
					<tr id="last_up" style="display:none">
						<th>Last Up</td>
						<td>At Least <?php 
								$carbon = Carbon::createFromTimeStamp($server->last_up - (60 * 60 * 11));
								echo $carbon->diffForHumans();
							?></td>
					</tr>
					<tr>
						<th>Uptime</td>
						<td>{{number_format(round(100 * $server->uptime_success / ($server->uptime_success + $server->uptime_fail), 2), 2) }}%</td>
					</tr>
					<tr>
						<th>IP</th>
						<td><code id="ip">{{ $server->ip }}{{ $server->port == 25565 ? '' : ':' . $server->port }}</code></td>
					</tr>
					@if ($server->website)
					<tr>
						<th>Website </th>
						<td><a href="{{ $server->website }}">{{ $server->website }}</a></td>
					</tr>
					@endif
					<tr>
						<th>Votifier</th>
						<td>{{ $votifier ? '<span class="label label-success">Enabled</span>' : '<span class="label">Not Enabled</span>' }}</td>
					</tr>
					<tr>
						<th rowspan="2">Players</td>
						<td class="ajaxd" id="players"><i class="muted">Loading</i></td>
					</tr>
					<tr>
						<td>
							<div class="progress" style="margin:0">
								<div class="bar" class="ajaxd" id="playerbar"></div>
							</div>
						</td>
					<tr>
						<th>Votes</td>
						<td class="ajaxd" id="players">{{ $votes }}</td>
					</tr>
					<tr>
						<th>Version</th>
						<td class="ajaxd" id="version"><i class="muted">Loading</i></td>
					</tr>
				</table>
			</div>
		</div>
		<div class="span6">
			@if ($role)
			<div class="boxxed padded">
				<a href="{{ URL::to('/server/' . $id . '/edit') }}" class="btn">Edit</a> &nbsp;
				<a href="{{ URL::to('/server/' . $id . '/delete') }}" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this posting?')">Delete</a> &nbsp; 
				@if (strtotime($server->sponsored) < time())
				<a href="{{ URL::to('/paypal/exit') }}" class="btn btn-primary">Sponsor for ${{ Config::get('app.sponsor_price_per_month') }}/mo</a> &nbsp;
				@endif

				@if ($role > 10)
				<a href="{{ URL::to('/server/' . $id . '/ban') }}" class="btn btn-danger">Ban User</a> &nbsp;
				@if (strtotime($server->sponsored) < time())
				<a href="#adminsponsorBox" class="btn btn-warning" data-toggle="modal">Sponsor Server</a>
				@else
				<a href="{{ URL::to('/server/' . $id . '?unsponsor') }}" class="btn btn-warning" data-toggle="modal">Unsponsor</a>
				@endif
				@endif
			</div>
			@endif
			<div class="boxxed padded">
				@if (count($images))
				<div class="server-img standard" style="background:url('{{ URL::to('/') }}/uploads/{{ $images[0]->filename }}');margin:0 auto 20px"></div>
				@endif
				<h2>Description</h2>
				{{ $server->description }}
			</div>
			@if ($server->plugins)
			<div class="boxxed">
				<h2 class="padded">Plugins</h2>
				<div class="row">
					<div class="span3">
						<table class="table table-striped">
							<thead>
								<td>Plugin</td>
								<td>Version</td>
							</thead>
							<tbody id="plugins1">
								<tr>
									<td>&nbsp;</td>
									<td><i class="muted">Loading</i></td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="span3">
						<table class="table table-striped">
							<thead>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</thead>
							<tbody id="plugins2">
							</tbody>
						</table>

					</div>
				</div>
			</div>
			@endif
		</div>
		<div class="span3">
			<div class="padded boxxed">
				<h2>Recent Votes</h2><br>
				@if (!count($vote_people))
				<b>No votes yet, be the first!</b>
				@else
				@for ($i = 0; $i < count($vote_people); $i++)
				@if ($i > 0 && floor($i * 0.2) == $i * 0.2)
				<br>
				@endif
				<img src="https://minotar.net/helm/{{ $vote_people[$i]->username }}/25.png" title="{{ $vote_people[$i]->username }}" class="avatar">
				@endfor
				@endif
			</div>
			@if (count($images) > 1)
			<div class="boxxed" style="padding:10px 0">
				<ul id="slides">
					@foreach ($images as $image)
					<li><img src="{{ URL::to('/') }}/uploads/{{ $image->filename }}" alt="Posted Image"></li>
					@endforeach
				</ul>
			</div>
			@endif
		</div>
	</div>
</div>
@if (!$hasvoted)

<div id="voteBox" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="voteBoxLabel" aria-hidden="true">
  <div class="modal-header">
    <h3 id="voteBoxLabel">Vote for Your Server!</h3>
  </div>
  <div class="modal-body">
    {{ Form::open(array('url' => 'server/' . Request::segment(2) . '/vote', 'method' => 'POST')) }}
    	<div class="form-horizontal">
			<div class="control-group">
				{{ Form::label('username', 'In-Game Name', array('class' => 'control-label')) }}
				<div class="controls">
					{{ Form::text('username') }}
				</div>
			</div>
			<div class="control-group">
				<div class="controls">
					{{ Form::captcha() }}
				</div>
			</div>
		</div>
  </div>
  <div class="modal-footer">
    <a class="btn" data-dismiss="modal" aria-hidden="true">Close</a>
    <input type="submit" value="Vote!" class="btn btn-primary">
    {{ Form::close() }}
  </div>
</div>

@endif

@if ($role > 10)

<div id="adminsponsorBox" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="adminsponsorBox" aria-hidden="true">
  <div class="modal-header">
    <h3 id="adminsponsorLabel">Manually Sponsor Server</h3>
  </div>
  <div class="modal-body">
    {{ Form::open(array('url' => 'server/' . Request::segment(2) . '/adminsponsor', 'method' => 'POST')) }}
    	<div class="form-horizontal">
			<div class="control-group">
				{{ Form::label('duration', 'Days to Sponsor', array('class' => 'control-label')) }}
				<div class="controls">
					{{ Form::text('duration') }}
				</div>
			</div>
		</div>
  </div>
  <div class="modal-footer">
    <a class="btn" data-dismiss="modal" aria-hidden="true">Close</a>
    <input type="submit" value="Sponsify!" class="btn btn-primary">
    {{ Form::close() }}
  </div>
</div>

@endif

<script type="text/javascript">
	window.serverurl = "{{ URL::to('/server/' . $id) }}";
</script>