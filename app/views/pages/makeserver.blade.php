<div class="container" id="content-standard">
	<div class="row">
		<div class="span8 boxxed">
			<div class="padded">
				<h1>Server Data</h1>
				{{ isset($alert) && !is_null($alert) ? $alert : '' }}
				{{ Form::open(array('url' => '/server/create', 'method' => 'POST')) }}
				@if ($edit)
				{{ Form::hidden('edit', $edit) }}
				@endif
					<div class="form-horizontal">
						<div class="control-group">
							{{ Form::label('name', 'Server Name*', array('class' => 'control-label')) }}
							<div class="controls">
								{{ Form::text('name', Input::get('name')) }}
							</div>
						</div>
						<div class="control-group">
							{{ Form::label('ip', 'IP Address*', array('class' => 'control-label')) }}
							<div class="controls">
								{{ Form::text('ip', Input::get('ip')) }}
							</div>
						</div>
						<div class="control-group">
							{{ Form::label('query_port', 'Query Port*', array('class' => 'control-label')) }}
							<div class="controls">
								{{ Form::text('query_port', Input::get('query_port', 25565)) }}
							</div>
						</div>
						<div class="control-group">
							{{ Form::label('website', 'Website', array('class' => 'control-label')) }}
							<div class="controls">
								{{ Form::text('website', Input::get('website')) }}
							</div>
						</div>
						<div class="control-group">
							{{ Form::label('description', 'Description*', array('class' => 'control-label')) }}
							<div class="controls">
								{{ Form::textarea('description', Input::get('description')) }}
								<span class="help-block">You can use <code>[b]bold[/b]</code>, <code>[i]italics[/i]</code>, and <code>[u]underline[/u]</code> tags, as well as making <code>-- Bullet Points</code>
							</div>
						</div>
						<div class="control-group">
							{{ Form::label('country', 'Country', array('class' => 'control-label')) }}
							<div class="controls">
								{{ Form::text('country', Input::get('country')) }}
							</div>
						</div>
						<div class="control-group">
							{{ Form::label('plugins', 'Show Plugins', array('class' => 'control-label')) }}
							<div class="controls">
								{{ Form::checkbox('plugins', 'yes', true) }}
							</div>
						</div>
						<div class="control-group">
							<div class="controls">
								<input type="submit" class="btn btn-primary" value="Create Server">
							</div>
						</div>
					</div>
			</div>
		</div>
		<div class="span4">
			<div class="padded boxxed">
				<div class="control-group">
					{{ Form::label('vf-port', 'Votifier Port (if any)', array('class' => 'control-label')) }}
					<div class="controls">
						{{ Form::text('vf-port', Input::get('vf-port')) }}
					</div>
				</div>
				<div class="control-group">
					{{ Form::label('vf-public', 'Votifier Public Key (if any)', array('class' => 'control-label')) }}
					<div class="controls">
						{{ Form::textarea('vf-public', Input::get('vf-public')) }}
					</div>
				</div>
				{{ Form::close() }}
			</div>
			<div class="padded boxxed">
				<h1>Upload Images</h1>
				<p>The first image you upload will be used as the banner. It should be <b>468 x 60</b>. If it is larger or smaller, it will be resized, but gif animations will not be saved. <b>The first image will be used as your banner. You may only upload four images.</b></p><br>	
				@if (count($images))
				
				<a href="{{ URL::current() }}?clearimg=true">Clear Images</a>

				@foreach ($images as $im)
				<div style="background:url('{{ URL::to('/') }}/uploads/{{ $im->filename }}') center center; height:90px"></div>
				@endforeach

				@else
				<form action="{{ url('/server/upload')}}" class="dropzone" id="serverupload"></form>
				@endif
			</div>
		</div>
	</div>
</div>