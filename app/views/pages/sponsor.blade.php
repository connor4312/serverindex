<div class="container" id="content-standard">
	<div class="row">
		<div class="span12 boxxed">
			<div class="padded">
				<h1>Sponsor Server</h1>
				<div class="alert alert-{{ $level }}">{{ $message }}</div>
			</div>
		</div>
	</div>
</div>