{{ Form::open(array('url' => '/users/' . $user->id . '/edit', 'method' => 'POST')) }}
<table class="table table-striped">
	<tr>
		<td>
		User ID
		</td>
		<td>
			<input type="text" value="{{ $user->id }}" disabled>
		</td>
	</tr>
	<tr>
		<td>
		{{ Form::label('username', 'Username') }}
		</td>
		<td>
			{{ Form::text('username', $user->username) }}
		</td>
	</tr>
	<tr>
		<td>
		{{ Form::label('email', 'Email') }}
		</td>
		<td>
			{{ Form::text('email', $user->email) }}
		</td>
	</tr>
	<tr>
		<td>
		{{ Form::label('password', 'Password') }}
		</td>
		<td>
			{{ Form::password('password') }}
			<div class="muted">Blank to not change</div>
		</td>
	</tr>
	<tr>
		<td>
		{{ Form::label('role', 'Role') }}
		</td>
		<td>
			{{ Form::select('role', array('' => 'None', 'superuser' => 'Superuser'), $user->role) }}
		</td>
	</tr>
	<tr>
		<td>
		{{ Form::label('status', 'status') }}
		</td>
		<td>
			{{ Form::select('status', array('active' => 'Active', 'banned' => 'Banned'), $user->status) }}
		</td>
	</tr>
	<tr>
		<td>
		Server
		</td>
		<td>
			@if ($server = $user->server)
			{{ HTML::link('server/' . $server->id, 'Server', array('target' => '_blank', 'class' => 'btn') )}}
			@else
			{{ HTML::link('', 'No Server', array('target' => '_blank', 'class' => 'btn disabled') )}}
			@endif
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>{{ Form::submit('Save', array('class' => 'btn btn-primary')) }}</td>
	</tr>
<table>
{{ Form::close() }}