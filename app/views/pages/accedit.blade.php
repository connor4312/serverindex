<div class="container" id="content-standard">
	<div class="row">
		<div class="span12 boxxed">
			<div class="padded">
				<h1>Edit Account</h1>

				{{ isset($msg) && !is_null($msg) ? '<div class="alert">' . $msg . '</div>' : '' }}
				{{ Form::open(array('url' => '/account/edit', 'method' => 'POST', 'class' => 'form-horizontal')) }}
					<div class="control-group">
						{{ Form::label('email', 'Email', array('class' => 'control-label')) }}
						<div class="controls">
							{{ Form::text('email', $user->email) }}
						</div>
					</div>
					<div class="control-group">
						{{ Form::label('password', 'New Password (or leave blank)', array('class' => 'control-label')) }}
						<div class="controls">
							{{ Form::password('password') }}
						</div>
					</div>
					<div class="control-group">
						{{ Form::label('currentpassword', 'Current Password', array('class' => 'control-label')) }}
						<div class="controls">
							{{ Form::password('currentpassword') }}
						</div>
					</div>
					<div class="control-group">
						<div class="controls">
							<input type="submit" class="btn btn-primary" value="Save">
						</div>
					</div>
				{{ Form::close() }}
			</div>
		</div>
	</div>
</div>