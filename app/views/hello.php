<!doctype html>
<html lang="en">
<head>
    <BODY bgcolor="#E2E2E2">
    <meta charset="UTF-8">
    <title>The Server Index</title>
    <style>
        @import url(http://fonts.googleapis.com/css?family=Lato:300,400,700);

        body {
            margin:0;
            font-family:'Lato', sans-serif;
            text-align:center;
            color: #999;
        }

        .welcome {
           width: 300px;
           height: 300px;
           position: absolute;
           left: 50%;
           top: 50%; 
           margin-left: -150px;
           margin-top: -200px;
        }

        .facebook {
           width: 40px;
           height: 39px;
           position: absolute;
           left: 50%;
           top: 50%; 
           margin-left: -50px;
           margin-top: 100px;
        }

        .twitter {
           width: 40px;
           height: 39px;
           position: absolute;
           left: 50%;
           top: 50%; 
           margin-left: 13px;
           margin-top: 100px;
        }

        a, a:visited {
            color:#E2E2E2;
            text-decoration:none;
        }

        a:hover {
            text-decoration:underline;
        }

        ul li {
            display:inline;
            margin:0 1.2em;
        }

        p {
            margin:2em 0;
            color:#555;
        }
    </style>
</head>
<body>
    <div class="facebook">
        <a href="https://www.facebook.com/pages/The-Server-Index/562605907122911" title="Facebook"><img class="facebook"src="http://puu.sh/3tUau.png"></a>
    </div>
    <div class="twitter">
        <a href="http://www.twitter.com/theserverindex" title="Twitter"><img class="twitter" src="http://puu.sh/3tUee.png"></a>
    </div>
    <div class="welcome">
        <a title="The Server Index"><img class="logo"src="http://i.imgur.com/dWjbVFa.png"></a>
        <h1>Coming Soon.</h1>
    <div>
</body>
</html>
