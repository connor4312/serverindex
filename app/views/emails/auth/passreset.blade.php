<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Password Reset</h2>

		<div>
			Hi, your Server Index password has been reset upon request. Your new password is <b>{{ $password }}</b>.
		</div>
	</body>
</html>