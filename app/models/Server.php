<?php


class Server extends Eloquent {

	public function votifier() {
		return $this->hasOne('Votifier');
	}

	public function user() {
		return $this->belongsTo('User');
	}

	public function votes() {
		return $this->hasMany('Vote');
	}

	public function image() {
		return $this->hasMany('Image');
	}

}