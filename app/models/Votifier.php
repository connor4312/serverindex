<?php


class Votifier extends Eloquent {

	protected $table = 'votifier';

	public function server() {
		return $this->belongsTo('Server');
	}

	public function fails() {
		return $this->hasMany('VFFail');
	}

}