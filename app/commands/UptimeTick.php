<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class UptimeTick extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'tsi:uptime';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Crons each server and registers uptime';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */
	public function fire()
	{
		$servers = DB::table('servers')->select('id', 'ip', 'port', 'uptime_success', 'uptime_fail')->get();
		$hundredth = count($servers) * 0.05;
		$next = $hundredth;
		$iteration = 5;


		for ($i=0; $i < count($servers); $i++) {
			if ($i>$next) {
				$this->info($iteration . '% done, processed ' . $i . ' servers.');
				$next += $hundredth;
				$iteration+= 5;
			}

			if ($fp = @fsockopen($servers[$i]->ip, $servers[$i]->port, $errCode, $errStr,1)) {
				DB::table('servers')->
					where('id', $servers[$i]->id)->
					update(array(
						'uptime_success' => $servers[$i]->uptime_success + 1,
						'last_up' => time()
					));
				fclose($fp);
			} else {
				DB::table('servers')->
						where('id', $servers[$i]->id)->
						update(array('uptime_fail' => $servers[$i]->uptime_fail + 1));
			}

		}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			
		);
	}

}