<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class VFRetry extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'tsi:voteretry';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Retries to send failed Votifier votes.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */
	public function fire()
	{
		$votes = DB::table('vffails')->
			join('votifier', 'vffails.votifier_id', '=', 'votifier_id')->
			select('votifier.public_key', 'votifier.ip', 'votifier.port', 'vffails.username')->
			get();

		foreach ($votes as $v) {
			VotifierHandler::Submit($v->public_key, $v->ip, $v->port, $v->username);
		}

		DB::table('vffails')->delete();
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
		);
	}

}