<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'IndexController@index');

Route::any('/account/login',  'LoginController@index');
Route::any('/account/logout',  'LoginController@index');
Route::post('/account/register',  'LoginController@register');
Route::post('/account/forgot',  'LoginController@forgot');

Route::get('/account/edit', 'AccountController@view');
Route::post('/account/edit', 'AccountController@edit');

Route::get('/account/server',  function() {

	if (!Auth::check()) {
		return Redirect::to('/account/login');
	}

	if ($s = Auth::user()->server) {
		return Redirect::to('/server/' . $s->id);
	} else {
		return Redirect::to('/server/create');
	}

});

Route::get('/server/create', 'ServerEditController@createServer');
Route::post('/server/create', 'ServerEditController@postServer');
Route::any('/server/upload', 'ServerEditController@uploadImg');

Route::get('/server/{id}', 'ServerController@view')->where('id', '[0-9]+');
Route::get('/server/{id}/query', 'ServerController@query')->where('id', '[0-9]+');
Route::post('/server/{id}/vote', 'ServerController@vote')->where('id', '[0-9]+');

Route::get('/server/{id}/delete', 'ServerController@delete')->where('id', '[0-9]+');
Route::get('/server/{id}/sponsored', 'ServerController@sponsor')->where('id', '[0-9]+');
Route::get('/server/{id}/ban', 'ServerController@ban')->where('id', '[0-9]+');
Route::any('/server/{id}/edit', 'ServerEditController@edit')->where('id', '[0-9]+');

Route::any('/server/{id}/adminsponsor', 'ServerController@sponsor')->where('id', '[0-9]+');

Route::get('/index', 'IndexController@index');

Route::get('/users', 'UsersController@index');
Route::get('/users/register', 'UsersController@register');
Route::get('/users/{id}/view', 'UsersController@view');
Route::post('/users/{id}/edit', 'UsersController@edit');

Route::any('/paypal/exit', 'PaymentController@exitpage');
Route::any('/paypal/enter', 'PaymentController@enter');

Route::get('/tos', function() {
	return View::make('tos');
});