SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

DROP TABLE IF EXISTS `tsi_images`;
CREATE TABLE IF NOT EXISTS `tsi_images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `server_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `filename` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=47 ;

DROP TABLE IF EXISTS `tsi_migrations`;
CREATE TABLE IF NOT EXISTS `tsi_migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `tsi_servers`;
CREATE TABLE IF NOT EXISTS `tsi_servers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `ip` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `port` int(11) NOT NULL,
  `query_port` int(8) NOT NULL DEFAULT '25565',
  `website` char(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sponsored` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `uptime_success` int(11) NOT NULL DEFAULT '1',
  `uptime_fail` int(11) NOT NULL DEFAULT '0',
  `last_up` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `votes` int(10) NOT NULL DEFAULT '0',
  `country` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `plugins` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=31 ;

DROP TABLE IF EXISTS `tsi_users`;
CREATE TABLE IF NOT EXISTS `tsi_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=34 ;

DROP TABLE IF EXISTS `tsi_vffails`;
CREATE TABLE IF NOT EXISTS `tsi_vffails` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `votifier_id` int(11) NOT NULL,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `tsi_votes`;
CREATE TABLE IF NOT EXISTS `tsi_votes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `server_id` int(11) NOT NULL,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ip` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1257 ;

DROP TABLE IF EXISTS `tsi_votifier`;
CREATE TABLE IF NOT EXISTS `tsi_votifier` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `server_id` int(11) NOT NULL,
  `ip` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `port` int(11) NOT NULL,
  `public_key` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

CREATE DEFINER=`root`@`localhost` EVENT `ClearVotes` ON SCHEDULE EVERY 1 MONTH STARTS '2013-08-01 00:00:00' ON COMPLETION NOT PRESERVE ENABLE DO DELETE FROM `tsi_votes`;

CREATE DEFINER=`root`@`localhost` EVENT `ClearServerVotes` ON SCHEDULE EVERY 1 MONTH STARTS '2013-08-01 00:00:00' ON COMPLETION NOT PRESERVE ENABLE DO UPDATE `tsi_servers` SET `votes` = 0;

CREATE DEFINER=`root`@`localhost` EVENT `ClearOldServers` ON SCHEDULE EVERY 1 DAY STARTS '2013-07-04 00:00:00' ON COMPLETION NOT PRESERVE DISABLE DO DELETE FROM `tsi_servers` WHERE `last_up` < (CURRENT_TIMESTAMP() - (60 * 60 * 24 * 30)) AND DATEDIFF(NOW(), `created_at`) > 30;

